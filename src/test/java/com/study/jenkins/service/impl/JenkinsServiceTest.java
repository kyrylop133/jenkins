package com.study.jenkins.service.impl;

import com.study.jenkins.exceptions.BadRequestException;
import com.study.jenkins.service.JenkinsService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class JenkinsServiceTest {

    private final JenkinsService jenkinsService = new JenkinsServiceImpl();

    @Test
    void addTest() {
        final Integer a = 2;
        final Integer b = 3;
        final Integer expectedResult = 5;
        final Integer actualResult = jenkinsService.add(a, b);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractTest() {
        final Integer a = null;
        final Integer b = 3;
        final Integer expectedResult = 2;
        final Integer actualResult = jenkinsService.subtract(a, b);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void subtractExceptionTest() {
        assertThrows(BadRequestException.class, () -> jenkinsService.subtract(null, null));
    }


}