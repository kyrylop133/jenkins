package com.study.jenkins.service.impl;

import com.study.jenkins.exceptions.BadRequestException;
import com.study.jenkins.service.JenkinsService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class JenkinsServiceImpl implements JenkinsService {

    @Override
    public Integer add(Integer a, Integer b) {
        if(Objects.isNull(a) || Objects.isNull(b)){
            throw new BadRequestException();
        }
        return a + b;

    }

    @Override
    public Integer subtract(Integer a, Integer b) {
        if(Objects.isNull(a) || Objects.isNull(b)){
            throw new BadRequestException();
        }
        return a - b;
    }
}
