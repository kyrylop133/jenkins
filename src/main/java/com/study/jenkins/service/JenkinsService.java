package com.study.jenkins.service;

public interface JenkinsService {

    Integer add(Integer a, Integer b);

    Integer subtract(Integer a, Integer b);
}
